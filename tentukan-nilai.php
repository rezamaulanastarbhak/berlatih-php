<!DOCTYPE html>
<html>
<head>
	<title>Tentukan Nilai</title>
</head>
<body>
	<?php
		function tentukan_nilai($nilai)
		{
		    if ( $nilai >= 85 && $nilai <= 100 ) {
				$predikat = 'Sangat Baik';
			} else if ( $nilai >= 70 && $nilai <= 85 ) {
				$predikat = 'Baik';
			} else if ( $nilai >= 60 && $nilai <= 70 ) {
				$predikat = 'Cukup';
			} else {
				$predikat = 'Kurang';
			}

			return $predikat ."<br>";
		}
		//TEST CASES
		echo tentukan_nilai(98); //Sangat Baik
		echo tentukan_nilai(76); //Baik
		echo tentukan_nilai(67); //Cukup
		echo tentukan_nilai(43); //Kurang
	?>
</body>
</html>